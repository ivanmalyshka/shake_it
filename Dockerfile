FROM python:3.10-slim-buster

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /shake-it

RUN apt-get update \
    && apt-get -y install libpq-dev gcc
	
COPY requirements.txt /shake-it/
RUN pip install -r requirements.txt
COPY . /shake-it/